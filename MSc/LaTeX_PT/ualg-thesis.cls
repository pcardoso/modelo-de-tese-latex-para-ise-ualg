%% Este ficheiro é o 'ualg-thesis.cls',
%% fornece o estilo para a escrita de Teses de mestrado 
%% na Universidade do Algarve
%% 
%% Este modelo é baseado 'uiucthesis.cls'
%% com \fileversion{v2.25} \def\filedate{2005/03/25}
%%
%% A documentação a usar será a do 'uiucthesis'
%% pelo menos por enquanto...
%% autor: Pedro Cardoso (pcardoso@ualg.pt)
%% Contribuições: João Pereira

\def\fileversion{v1.0} \def\filedate{2008/06/09}
%% Package and Class "adee-ise-ualg-thesis" for use with LaTeX2e.
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{ualg-thesis}
[\filedate\ \fileversion\ ualg-thesis (PJSC)]
\RequirePackage{setspace}
% \RequirePackage{ualg-thesis}

% limpa páginas vazias de headers e footers
\typeout{Limpa páginas vazias}
\let\cleardouble@page\cleardoublepage
\AtBeginDocument{%
\ifx\cleardouble@page\cleardoublepage
 \def\cleardoublepage{\clearpage
   {\pagestyle{empty}\cleardouble@page}}%
\fi
}

\expandafter\ifx\csname @ifundefined\endcsname\relax
\def\@ifundefined#1{%
	\expandafter\ifx\csname#1\endcsname\relax
	\expandafter\@firstoftwo
	\else
	\expandafter\@secondoftwo
	\fi}
\fi
\@ifundefined{MakeUppercase}{\let\MakeUppercase=\uppercase}{}
\newif\if@thesisdraft \@thesisdraftfalse
\newif\if@thesisfancy \@thesisfancyfalse
\newif\if@fullpage \@fullpagefalse
\newif\if@largecaps \@largecapsfalse
\newif\if@proquest \@proquestfalse
\newif\if@edeposit \@edepositfalse
\newif\if@thesisoffcenter \@thesisoffcenterfalse
\newif\if@centerchapter \@centerchapterfalse
\DeclareOption{draftthesis}{\@thesisdrafttrue}
\DeclareOption{fancy}{\@thesisfancytrue}
\DeclareOption{fullpage}{\@fullpagetrue}
\DeclareOption{proquest}{\@proquesttrue}
\DeclareOption{toclabels}{\AtBeginDocument{\toclabels}}
\DeclareOption{edeposit}{\@edeposittrue}
\DeclareOption{offcenter}{\@thesisoffcentertrue}
\DeclareOption{centerchapter}{\@centerchaptertrue}
\DeclareOption{largecaps}{\@largecapstrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\PassOptionsToClass{letterpaper,oneside}{book}
\ProcessOptions
\LoadClass{book}
\if@proquest
\nofiles    % don't overwrite the .aux files
\def\makeindex{}
\@thesisfancyfalse
\@fullpagetrue
\fi
\if@thesisdraft
\newcount\timehh\newcount\timemm
\timehh=\time \divide\timehh by 60
\timemm=\time \count255=\timehh \multiply\count255 by -60
\advance\timemm by \count255
\def\draftheader{\slshape Draft of \today\ at
	\ifnum\timehh<10 0\fi\number\timehh\,:\,\ifnum\timemm<10 0\fi\number\timemm}%
\fi
\newcommand{\toclabels}{%
	\addtocontents{toc}{\vspace*{-\baselineskip}\hfill Page\endgraf}%
	\addtocontents{lof}{\vspace*{-\baselineskip}~Figure\hfill Page\endgraf}%
	\addtocontents{lot}{\vspace*{-\baselineskip}~Table\hfill Page\endgraf}}
\def\@mkuptitle#1{\gdef\@Utitle{#1}}
\def\@mkupsubtitle#1{\gdef\@Usubtitle{#1}}
\def\title#1{\gdef\@title{#1}}

%\def\subtitle#1{\gdef\@subtitle{#1}}
\def\subtitle#1{\gdef\@subtitle{#1}\MakeUppercase{\protect\@mkupsubtitle{#1}}}
%\def\USubtitle{\MakeUppercase\protect{\@subtitle}}
\def\@mkupsubtitle#1{\gdef\USubtitle{#1}}

\def\@mkupauthor#1{\gdef\@Uauthor{#1}}
\def\UTitle{\MakeUppercase{\@title}}

\def\author#1{\gdef\@author{#1}\MakeUppercase{\protect\@mkupauthor{#1}}}
\def\phdthesis{\def\@degree{Doctor of Philosophy}
	\def\degree{Ph.D.}
	\def\@thesisname{Disserta\c c\~ao}
}
\def\msthesis{\def\@degree{Mestre}
	\def\degree{Mestrado em}
	\def\@thesisname{\Large Disserta\c c\~ao}
}
\newcommand{\otherdoctorate}[2]{\def\@degree{#1}
	\def\degree{#2}
	\def\@thesisname{Disserta\c c\~ao}
}
\newcommand{\othermasters}[2]{\def\@degree{#1}
	\def\degree{#2}
	\def\@thesisname{TESE}
}
\def\department#1{\def\@dept{#1}}
\def\college#1{\def\@college{#1}}
\def\schools#1{\def\@schools{#1}}
\def\course#1{\def\@course{#1}}
\newcommand{\specialty}[1]{\def\@specialty{#1}}
\def\degreeyear#1{\def\@degreeyear{#1}}
\newcommand{\committee}[1]{\gdef\@committee{#1}}
\newcommand*{\volume}[1]{\gdef\thesis@volume{VOLUME~#1}}
\newcommand*{\thesis@volume}{}
\if@edeposit
\gdef\@committee{%
	\ClassError{uiucthesis}{A committee must be specified for e-deposit dissertations.}%
	{Use \protect\committee\space with members separated by \protect\\'s.}}
\fi

\def\copyrightnotice{\copyright~\@degreeyear~Todos os direitos reservados.}
\def\copyrightnotice{\large \@degreeyear}
\newif\if@thesiscrpage \@thesiscrpagetrue
\let\nocopyrightpage\@thesiscrpagefalse
\if@thesisdraft\nocopyrightpage\fi
\phdthesis
%\department{Departamento de Engenharia Eletrot\'ecnica\\Instituto Superior de Engenharia\\Universidade do Algarve}
%\college{DEE}
\def\@schools{}
\def\@degreeyear{\number\year}

\newcommand\makecopyrightpage{
	%     \thispagestyle{empty}% 
	\enlargethispage{1in}%
	\begingroup
	\def\baselinestretch{1}
	\ifnum \@ptsize=2
	\@normalsize
	\newcommand{\thesis@small}{\small}
	\else
	\large
	\newcommand{\thesis@small}{\@normalsize}
	\fi
	\headheight=0pt \headsep=0pt
	\topmargin=0in
	\@tempdima=\paperwidth
	\advance\@tempdima by -\textwidth
	\divide\@tempdima by 2
	\advance\@tempdima by -1in
	\oddsidemargin=\@tempdima
	\let\evensidemargin=\oddsidemargin
	\newdimen\thesis@dim
	\if@edeposit
	\thesis@dim=1.25in
	\else
	\thesis@dim=1.75in
	\fi
	\def\lc@selectfont{\fontfamily{phv}\selectfont}
	\cleardoublepage
	\begin{center}
		\vspace*{2cm}
		\begin{onehalfspacing}
			
			{\Large\UTitle}\\[.5cm]
            
			{\large\USubtitle}
			%\protect\\
		\end{onehalfspacing}
        
		\vspace{1cm}
		\textbf{{\large Declara\c c\~ao de autoria de trabalho}}
	\end{center}            
	
	%	\noindent Declaro ser o autor deste trabalho, que é original e inédito. Autores e trabalhos consultados estão devidamente citados no texto e constam da listagem de referências incluída. \\[2cm]
	\noindent Declaro ser o autor deste trabalho, que é original e inédito. Autores e trabalhos consultados estão devidamente citados no texto e constam da listagem de referências incluída. \\[0.4cm]
	\begin{center}
		\vspace{1cm}
		\textbf{{\large \it Declaration of authorship of the work}}
	\end{center} 	
	
	\noindent {\itshape I hereby declare to be the author of this work, which is original and unpublished. Authors and works consulted are properly cited in the text and included in the reference list.}\\[2cm]
	
	% \noindent \textit{ Declaro ser o autor deste trabalho, que \'e original e in\'edito. Autores e trabalhos consultados est\~ao devidamente citados no texto e constam da listagem de refer\^encias inclu\'\i da.} 
	
	%		\begin{flushright}
	\begin{center}
		\rule{\textwidth}{0.4pt}\\[0.25cm]
		({\@author})
	\end{center}
	
	%		\end{flushright}
	
%	\clearpage
	\vspace*{\fill}
	\begin{center}
		\large \copyright \@degreeyear, {\lc@selectfont\@Uauthor}\\[1cm]
	\end{center}
	
	A Universidade do Algarve reserva para si o direito, em conformidade com o disposto no Código do Direito de Autor e dos Direitos Conexos, de arquivar, reproduzir e publicar a obra, independentemente do meio utilizado, bem como de a divulgar através de repositórios científicos e de admitir a sua cópia e distribuição para fins meramente educacionais ou de investigação e não comerciais, conquanto seja dado o devido crédito ao autor e editor respetivos.\\[0.4cm]
	
	{\itshape The University of the Algarve reserves the right, in accordance with the terms of the Copyright and Related Rights Code, to file, reproduce and publish the work, regardless of the methods used, as well as to publish it through scientific repositories and to allow it to be copied and distributed for purely educational or research purposes and never for commercial purposes, provided that due credit is given to the respective author and publisher.}
	
	\clearpage
	\endgroup
}

\renewcommand\maketitle{
    \setcounter{page}{1}
	\thispagestyle{empty}%	
	\enlargethispage{44pt}%
	\begingroup
	\def\baselinestretch{1}
	\ifnum \@ptsize=2
	\@normalsize
	\newcommand{\thesis@small}{\small}
	\else
	\large
	\newcommand{\thesis@small}{\@normalsize}
	\fi
	\def\lc@selectfont{\fontfamily{phv}\selectfont}
	\begin{center}
		\vbox to 7cm{%
			{\large\bfseries\lc@selectfont\@Uauthor}
			\vfil
		}%
		\vbox to 4cm {%
			\begin{doublespacing}
				
				{\Large\bfseries\lc@selectfont\UTitle} \\[1cm]
				{\large\lc@selectfont\USubtitle}
			\end{doublespacing}
			\vfill
		}%

		\vbox to 2in{%
			\begin{flushright}
				{\bfseries\lc@selectfont\@course}\\
				\ifdefined\@specialty
				{\bfseries\lc@selectfont ~\@specialty}
				\fi
				\\[1cm]
				{\bfseries\lc@selectfont \@advisor}
				\vfil
			\end{flushright}
		}
		\vfill
		\centering
		\includegraphics[width=0.2\textwidth]{imagens/logo-ualg}\\[0.5cm]
		\lc@selectfont\large {\bfseries UNIVERSIDADE DO ALGARVE}\\
		\lc@selectfont{\bfseries \@schools}\\
		\@degreeyear

	\end{center}
	\newpage
	\endgroup
}


\newcommand\makecover{
	\thispagestyle{empty}%
	\enlargethispage{44pt}%
	\begingroup
	\def\baselinestretch{1}
	\ifnum \@ptsize=2
	\@normalsize
	\newcommand{\thesis@small}{\small}
	\else
	\large
	\newcommand{\thesis@small}{\@normalsize}
	\fi
	\def\lc@selectfont{\fontfamily{phv}\selectfont}
	
	\begin{center}
		\vbox to 7cm{%
			{\large\bfseries\lc@selectfont\@Uauthor}
			\vfil
		}%
		\vbox to 4cm {%
			\begin{doublespacing}
				
				{\Large\bfseries\lc@selectfont\UTitle} \\[.5cm]
				{\large\lc@selectfont\USubtitle}
			\end{doublespacing}
			\vfill
		}%
		\vbox to 2in{%
		}
		\vfill
		\centering
		\includegraphics[width=0.2\textwidth]{imagens/logo-ualg}\\[0.5cm]
		\lc@selectfont\large {\bfseries UNIVERSIDADE DO ALGARVE}\\
		\lc@selectfont{\bfseries \@schools}\\
		\@degreeyear \\[1cm]
	\end{center}
	\newpage
	\endgroup
}


\let\thesis@frontmatter=\frontmatter
\def\frontmatter{%
	\thesis@frontmatter
%	\if@edeposit
%	   \setcounter{page}{1}
%	\else
%	   \setcounter{page}{2}
%	\fi
    }
\renewcommand\contentsname{Table of Contents}
\renewcommand*\l@chapter[2]{%
	\ifnum \c@tocdepth >\m@ne
	\addpenalty{-\@highpenalty}%
	\vskip 1.0em \@plus 0.2em \@minus 0.2em
	\setlength\@tempdima{1.5em}
	\begingroup
	\leftskip \z@ \rightskip \@tocrmarg \parfillskip -\rightskip
	\parindent \z@
	\leavevmode \bfseries
	\advance\leftskip\@tempdima
	\hskip -\leftskip
	#1\nobreak
	\leaders\hbox{$\m@th\mkern\@dotsep mu\hbox{.}\mkern\@dotsep mu$}
	\hfil \nobreak\hbox to\@pnumwidth{\hss #2}\par
	\penalty\@highpenalty
	\endgroup
	\fi}
\let\thesis@tableofcontents=\tableofcontents
\def\tableofcontents{{\singlespacing\thesis@tableofcontents}}
\let\thesis@listoftables=\listoftables
\def\listoftables{\newpage%
	{\singlespacing\thesis@listoftables}
	\addcontentsline{toc}{chapter}{\listtablename}%
	}
\let\thesis@listoffigures=\listoffigures
\def\listoffigures{\newpage%
	{\singlespacing\thesis@listoffigures}
	\addcontentsline{toc}{chapter}{\listfigurename}%
	}
\newcommand*{\advisor}[1]{\gdef\@advisor{#1}}
\newcommand*{\adviser}[1]{\gdef\@advisor{#1}}
\def\abstractname{Abstract}
% \def\makecopyrightpage{}
\if@proquest
\def\maketitle{}
%  \def\makecopyrightpage{}
\def\@advisor{%
	\ClassError{uiucthesis}{An advisor must be specified for the ProQuest abstract}%
	{Use \protect\advisor\space to specify a name}}
\newenvironment{abstract}{%
	\newpage
	\pagestyle{empty}
	\setcounter{page}{1}
	\begin{singlespace}\begin{center}
			\@Utitle\\[\baselineskip]
			\@author, \degree\\
			Department of \@dept\\
			University of Illinois at Urbana-Champaign, \@degreeyear\\
			\@advisor, Adviser\\[\baselineskip]
	\end{center}\end{singlespace}\par\noindent\ignorespaces
}{
	\newpage
	\aftergroup\enddocument
	\aftergroup\endinput
}
\else
\newenvironment{abstract}{\chapter*{Abstract}}{}
\newenvironment{resumo}{\chapter*{Resumo}}{}
\fi

\newenvironment{dedication}{
	\newpage
	\leavevmode\vfill
	\begin{center}
		\itshape
	}{
	\end{center}
	\vskip 3ex
	\vfill
	\newpage
}

%\let\thesis@listoftables=\listoftables
%\def\listoftables{\newpage%
%	\addcontentsline{toc}{chapter}{\listtablename}%
%	{\singlespacing\thesis@listoftables}}

\newenvironment*{symbollist}[1][1in]{
	\begin{list}{}{\singlespacing
			\setlength{\leftmargin}{#1}
			\setlength{\labelwidth}{#1}
			\addtolength{\labelwidth}{-\labelsep}
			\setlength{\topsep}{0in}}%
		\def\makelabel##1{\hfil##1\hfil}%
	}{
\end{list}}

\newenvironment*{symbollist*}[1][1in]{
	\begin{symbollist}[#1]
		\def\makelabel##1{##1\hfil}}
	{\end{symbollist}}
\if@thesisfancy
\font\cminch=cminch at 60pt
\newcommand\chapternumberfont{\cminch}
\else
\newcommand\chapternumberfont{\huge\bfseries}
\fi
\newcommand\chaptertitlefont{\Huge\bfseries}
\def\@chapter[#1]#2{%
	\ifnum \c@secnumdepth >\m@ne
	\if@mainmatter
	\refstepcounter{chapter}%
	\typeout{\@chapapp\space\thechapter.}%
	\if@thesisfancy
	\addcontentsline{toc}{chapter}%
	{\protect\numberline{\thechapter}#1}%
	\else
	\addcontentsline{toc}{chapter}%
	%%% alterado por pcardoso (2023)
	%{\@chapapp\ \thechapter\quad #1}% %% escreve "capitulo" / "chapter" na tabela de conteudps 
	{\thechapter\quad #1}%
	%%%
	\fi
	\else
	\addcontentsline{toc}{chapter}{#1}%
	\fi
	\else
	\addcontentsline{toc}{chapter}{#1}%
	\fi
	\chaptermark{#1}%
	\addtocontents{lof}{\protect\addvspace{10\p@}}%
	\addtocontents{lot}{\protect\addvspace{10\p@}}%
	\if@twocolumn
	\@topnewpage[\@makechapterhead{#2}]%
	\else
	\@makechapterhead{#2}%
	\@afterheading
	\fi}
\newskip\thesis@chapskip
\AtBeginDocument{%
	\newdimen\chapternumberheight
	\begingroup
	\chapternumberfont
	\setbox255=\hbox{A}
	\if@thesisfancy
	\global\thesis@chapskip=\ht255
	\else
	\global\thesis@chapskip=\baselineskip
	\fi
	\dimen255=\ht255
	\chaptertitlefont
	\setbox255=\hbox{A}
	\advance\dimen255 by \ht255
	\if@thesisfancy
	\global\advance\thesis@chapskip by -\ht255
	\global\divide\thesis@chapskip by 2
	\global\advance\thesis@chapskip by 10\p@
	\else
	\global\advance\thesis@chapskip by 20\p@
	\fi
	\divide\dimen255 by 2
	\global\chapternumberheight=\dimen255
	\endgroup}
\newlength{\chaptertitleheight}
\if@thesisfancy
\setlength{\chaptertitleheight}{1.5in}
\else
\setlength{\chaptertitleheight}{1.85in}
\fi
\def\@makechapterhead#1{%
	\vbox to \chaptertitleheight{
		\def\baselinestretch{1}\@normalsize
		\parindent \z@ \raggedright \normalfont
		\if@centerchapter
		\centering
		\fi
		\ifnum \c@secnumdepth >\m@ne
		\if@mainmatter
		\thesis@chapskip=\z@
		\if@thesisfancy
		\vspace*{10\p@}%
		\leavevmode\llap{\vbox to \chapternumberheight{\hbox{%
					\chapternumberfont\thechapter\,}\vss}}%
		\else
		{\chapternumberfont \@chapapp\space \thechapter}
		\par\nobreak
		\vskip 20\p@
		\fi
		\fi
		\fi
		\interlinepenalty\@M
		\vspace*{\thesis@chapskip}%
		\chaptertitlefont #1
		\vfil
	}%
	\par\nobreak%
}
\def\@makeschapterhead#1{%
	\vbox to \chaptertitleheight{
		\def\baselinestretch{1}\@normalsize
		\parindent \z@ \raggedright \normalfont
		\if@centerchapter
		\centering
		\fi
		\interlinepenalty\@M
		\vspace*{\thesis@chapskip}
		\chaptertitlefont #1
		\vfil
	}%
	\par\nobreak%
}
\newcommand{\sectiontitlefont}{\Large\bfseries}
\newcommand{\subsectiontitlefont}{\large\bfseries}
\newcommand{\subsubsectiontitlefont}{\normalsize\bfseries}
\renewcommand\section{\@startsection {section}{1}{\z@}%
	{-3.5ex \@plus -1ex \@minus -.2ex}%
	{2.3ex \@plus.2ex}%
	{\raggedright\normalfont\sectiontitlefont}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
	{-3.25ex\@plus -1ex \@minus -.2ex}%
	{1.5ex \@plus .2ex}%
	{\raggedright\normalfont\subsectiontitlefont}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
	{-3.25ex\@plus -1ex \@minus -.2ex}%
	{1.5ex \@plus .2ex}%
	{\raggedright\normalfont\subsubsectiontitlefont}}
\let\thesis@appendix\appendix
\renewcommand\appendix{\thesis@appendix\@ifstar{\gdef\thechapter{}}{}}
\renewcommand\bibname{References}
\AtBeginDocument{\let\thesis@thebib\thebibliography
	\let\thesis@endbib\endthebibliography
	\def\thebibliography{\begingroup\singlespacing%
		\chapter*{\bibname}%
		\let\chapter\@gobbletwo%
		\thesis@thebib}
	\def\endthebibliography{\thesis@endbib\endgroup}}
\let\thesis@theindex=\theindex
\def\theindex{\addcontentsline{toc}{chapter}{\indexname}%
	\begingroup\singlespacing\thesis@theindex}
\let\thesis@endtheindex=\endtheindex
\def\endtheindex{\thesis@endtheindex\endgroup}
\topmargin=0pt
\advance \topmargin by -\headheight
\advance \topmargin by -\headsep
\textheight 8.9in
\if@fullpage
\setlength{\textwidth}{\paperwidth}
\addtolength{\textwidth}{-2in}
\@settopoint\textwidth
\fi
\setlength{\@tempdima}{\paperwidth}
\addtolength{\@tempdima}{-\textwidth}
\setlength{\oddsidemargin}{.5\@tempdima}
\addtolength{\oddsidemargin}{-1in}
\if@thesisoffcenter
\addtolength{\oddsidemargin}{0.5in}
\reversemarginpar
\fi
\setlength{\marginparwidth}{\oddsidemargin}
\addtolength{\marginparwidth}{1in}
\addtolength{\marginparwidth}{-\marginparsep}
\addtolength{\marginparwidth}{-24pt}
\@settopoint\oddsidemargin
\@settopoint\marginparwidth
\setlength{\evensidemargin}{\oddsidemargin}
\if@thesisdraft
\newcommand{\note}[1]{\marginpar{\def\baselinestretch{1}\small\raggedright #1}}
\else
\newcommand{\note}[1]{}
\let\thesis@marginpar\marginpar
\def\marginpar{%
	\ClassWarning{uiucthesis}{Margin paragraphs fall outside the allowed margins\MessageBreak
		for UIUC Theses, use \protect\note\space instead of \protect\marginpar.}%
	\thesis@marginpar}
\fi
\def\ps@plain{%
	\let\@mkboth\@gobbletwo
	\if@thesisdraft
	\def\@oddhead{\draftheader\hfil}
	\else
	\let\@oddhead\@empty
	\fi
	\let\@evenhead\@oddhead
	\def\@oddfoot{\reset@font\hfil\thepage\hfil}%
	\let\@evenfoot\@oddfoot
}
\if@twoside
\def\ps@headings{%
	\if@thesisdraft
	\def\@oddhead{\draftheader\hfil\slshape\rightmark}
	\def\@evenhead{\slshape\leftmark\hfil\draftheader}
	\else
	\def\@oddhead{\hfil\slshape\rightmark}
	\def\@evenhead{\slshape\leftmark\hfil}
	\fi
	\def\@oddfoot{\reset@font\hfil\thepage\hfil}%
	\let\@evenfoot\@oddfoot
	\let\@mkboth\markboth
	\if@thesisdraft
	\def\chaptermark##1{%
		\markboth {\MakeUppercase{%
				\ifnum \c@secnumdepth >\m@ne
				\if@mainmatter
				\@chapapp\ \thechapter%
				\fi
				\fi}}{}}
	\else
	\def\chaptermark##1{%
		\def\@chaphead{\MakeUppercase{%
				\ifnum \c@secnumdepth >\m@ne
				\if@mainmatter
				\if@thesisfancy
				\thechapter.~~%
				\else
				\@chapapp\ \thechapter.~~%
				\fi
				\fi
				\fi
				##1}}%
		\markboth{\@chaphead}{\@chaphead}}
	\fi
	\def\sectionmark##1{%
		\markright {\MakeUppercase{%
				\ifnum \c@secnumdepth >\z@
				\thesection. \ %
				\fi
				##1}}}}
\else
\def\ps@headings{%
	\if@thesisdraft
	\def\@oddhead{\draftheader\hfil\slshape\rightmark}
	\else
	\def\@oddhead{\hfil\slshape\rightmark\hfil}
	\fi
	\let\@evenhead\@oddhead
	\def\@oddfoot{\reset@font\hfil\thepage\hfil}%
	\let\@evenfoot\@oddfoot
	\let\@mkboth\markboth
	\if@thesisdraft
	\def\chaptermark##1{%
		\markright {\MakeUppercase{%
				\ifnum \c@secnumdepth >\m@ne
				\if@mainmatter
				\@chapapp\ \thechapter%
				\fi
				\fi}}}
	\else
	\def\chaptermark##1{%
		\markright {\MakeUppercase{%
				\ifnum \c@secnumdepth >\m@ne
				\if@mainmatter
				\if@thesisfancy
				\thechapter.~~%
				\else
				\@chapapp\ \thechapter.~~%
				\fi
				\fi
				\fi
				##1}}}
	\fi
}
\fi
%\pagestyle{plain}
\pagestyle{plain}
\renewcommand\chapter{\if@openright\cleardoublepage\else\clearpage\fi
	\@mkboth{}{}
	\thispagestyle{plain}
	\global\@topnum\z@
	\@afterindentfalse
	\secdef\@chapter\@schapter}
\newcommand\vitaname{Vita}
\newcommand\vita{
	\chapter{\vitaname}%
}
\def\thesisspacing{\if@fullpage\doublespacing\else\onehalfspacing\fi}
\pagenumbering{roman}
\AtBeginDocument{\thesisspacing}
\def\preliminary{\frontmatter}
\let\endpreliminary=\relax
\def\thesis{\mainmatter}
\let\endthesis=\relax
\endinput




%%
%% End of file `uiucthesis.cls'.
