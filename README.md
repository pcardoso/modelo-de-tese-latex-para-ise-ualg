# README #

## PT ##

Modelo atualizado de acordo com o Regulamento dos ciclos de estudos conducentes aos graus de mestre e de doutor da Universidade do Algarve publicado em Di�rio da Rep�blica

Regulamento n.o 114/2023, Di�rio da Rep�blica, 2.a s�rie, 23 de janeiro de 2023 [regulamento_2023.pdf](regulamento_2023.pdf)


## D�vidas ## 
Em caso de d�vidas falar com Pedro Cardoso (pcardoso@ualg.pt)


## EN ##

Updated model in accordance with the Regulation of study cycles leading to master's and doctor's degrees at the University of Algarve published in Di�rio da Rep�blica

Regulamento n.o 114/2023, Di�rio da Rep�blica, 2.a s�rie, 23 de janeiro de 2023 [regulamento_2023.pdf](regulamento_2023.pdf)
##  Doubts ## 
If you have any questions, speak to Pedro Cardoso (pcardoso@ualg.pt)